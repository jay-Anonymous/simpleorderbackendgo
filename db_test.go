package main

func TestDeleteCustomer(t *testing.T) {
	// Arrange
	service := NewCustomerService()
	customer := &Customer{ID: 1, Name: "John Doe", Email: "johndoe@example.com"}
	err := service.AddCustomer(customer)
	assert.Nil(t, err)

	// Act
	result, err := service.DeleteCustomer(customer.ID)

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, customer, result)
	// Check if the customer is deleted from the database
	customer, err = service.GetCustomerByID(customer.ID)
	assert.NotNil(t, err)
	assert.Nil(t, customer)
}


