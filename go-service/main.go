package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type Customer struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Code string `json:"code"`
}

type Order struct {
	ID         int       `json:"id"`
	CustomerID int       `json:"customer_id"`
	Item       string    `json:"item"`
	Amount     float64   `json:"amount"`
	Time       time.Time `json:"time"`
}

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("postgres", "user=postgres password=mysecretpassword dbname=mydb sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	sqlStmt, err := db.Prepare("CREATE TABLE IF NOT EXISTS Customers (id SERIAL PRIMARY KEY, name TEXT NOT NULL, code TEXT NOT NULL UNIQUE)")
	if err != nil {
		log.Fatal(err)
	}
	sqlStmt.Exec()

	sqlStmt, err = db.Prepare("CREATE TABLE IF NOT EXISTS Orders (id SERIAL PRIMARY KEY, customer_id INTEGER NOT NULL REFERENCES Customers (id), item TEXT NOT NULL, amount REAL NOT NULL, time TIMESTAMP NOT NULL)")
	if err != nil {
		log.Fatal(err)
	}
	sqlStmt.Exec()
}

func createCustomer(w http.ResponseWriter, r *http.Request) {
	var customer Customer
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&validCustomerustomer); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	sqlStmt, err := db.Prepare("INSERT INTO Customers (name, code) VALUES ($1, $2) RETURNING id")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = sqlStmt.QueryRow(customer.Name, customer.Code).Scan(&customer.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(customer)
}

func getCustomers(w http.ResponseWriter, r *http.Request) {
	rows, err := db.Query("SELECT id, name, code FROM Customers")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var customers []Customer
	for rows.Next() {
		var customer Customer
		err := rows.Scan(&customer.ID, &customer.Name, &customer.Code)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		customers = append(customers, customer)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(customers)
}

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.Atoi(params["id"])

	var customer Customer
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&validCustomer); err != nil || !validCustomer(&validCustomer) {
		http.Error(w, "Invalid request body.", http.StatusBadRequest)
		return
	}
	res, err := db.Exec(`UPDATE Customers SET name=$1, code=$2 WHERE id=$3`, customer.Name, customer.Code, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else if affected, _ := res.RowsAffected(); affected ==
		0 {
		http.NotFound(w, r)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}


}

