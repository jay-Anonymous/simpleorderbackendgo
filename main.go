package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

type Customer struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Code string `json:"code"`
}

type Order struct {
	ID       int    `json:"id"`
	CustomerID int    `json:"customer_id"`
	Item     string `json:"item"`
	Amount   float64 `json:"amount"`
	Time     string `json:"time"`
}

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("sqlite3", "./customers.db")
	if err != nil {
		panic(err)
	}

	sqlStmt, err := db.Prepare("CREATE TABLE Customers (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, code TEXT NOT NULL UNIQUE)")
	if err != nil {
		panic(err)
	}
	sqlStmt.Exec()

	sqlStmt, err = db.Prepare("CREATE TABLE Orders (id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id INTEGER NOT NULL, item TEXT NOT NULL, amount REAL NOT NULL, time TEXT NOT NULL, FOREIGN KEY (customer_id) REFERENCES Customers (id))")
	if err != nil {
		panic(err)
	}
	sqlStmt.Exec()
}

func createCustomer(c *gin.Context) {
	var customer Customer
	if err := c.BindJSON(&customer); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := db.Exec("INSERT INTO Customers (name, code) VALUES (?, ?)", customer.Name, customer.Code)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	customer.ID = int(id)
	c.JSON(http.StatusCreated, customer)
}

func getCustomers(c *gin.Context) {
	rows, err := db.Query("SELECT id, name, code FROM Customers")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	var customers []


	