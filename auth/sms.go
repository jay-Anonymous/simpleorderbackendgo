package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type AfricasTalkingResponse struct {
	SMSMessage struct {
		SMSMessageData struct {
			Message    string `json:"message"`
			Recipients []struct {
				Number string `json:"number"`
				Status string `json:"status"`
			} `json:"recipients"`
			Cost int `json:"cost"`
		} `json:"SMSMessageData"`
	} `json:"SMSMessage"`
}

type Order struct {
	CustomerName string `json:"customer_name"`
	// other order fields...
}

func sendSMS(to, message string) error {
	const url = "https://api.sandbox.africastalking.com/version1/messaging"

	type Request struct {
		Messages []struct {
			Recipient string `json:"to"`
			Message   string `json:"message"`
		} `json:"messages"`
	}

	req := &Request{
		Messages: []struct {
			Recipient string `json:"to"`
			Message   string `json:"message"`
		}{{to, message}},
	}

	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var atResp AfricasTalkingResponse
	err = json.Unmarshal(body, &atResp)
	if err != nil {
		return err
	}

	if atResp.SMSMessage.SMSMessageData.Cost == 0 {
		return fmt.Errorf("failed to send SMS")
	}

	return nil
}

func newOrderHandler(w http.ResponseWriter, r *http.Request) {
	// parse request body and create a new order
	var order Order
	err := json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// create the order...

	// send SMS to customer
	err = sendSMS("+2547xxxxxxxx", fmt.Sprintf("Hello %s, your order has been received.", order.CustomerName))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func main() {
	http.HandleFunc("/orders", newOrderHandler)

	http.ListenAndServe(":8080", nil)
}
