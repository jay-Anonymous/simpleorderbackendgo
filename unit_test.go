package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddCustomer(t *testing.T) {
	// Arrange
	service := NewCustomerService()

	// Act
	result, err := service.AddCustomer("John Doe", "johndoe@example.com")

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, "John Doe", result.Name)
	assert.Equal(t, "johndoe@example.com", result.Email)
}
